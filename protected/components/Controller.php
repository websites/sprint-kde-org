<?php
Class Controller Extends CController {
	protected $_menu = Array();
	public $breadcrumbs = Array();

	public function beforeAction() {
		$this->_menu[] = array(
			'label' => Yii::t('sprint', 'Upcoming sprints'),
			'url' => array('/sprint')
		);

		$this->_menu[] = array(
			'label' => Yii::t('sprint', 'All sprints'),
			'url' => array('/sprint/all')
		);

		$user = User::model()->findbyUid(Yii::app()->user->id);

		if($user && $user->isAdmin) {
			$this->_menu[] = array(
				'label' => Yii::t('sprint', 'Manage locations'),
				'url' => array('/location/admin')
			);

			$this->_menu[] = array(
				'label' => Yii::t('sprint', 'Manage sprints'),
				'url' => array('/sprint/admin')
			);
		}

		return True;
	}

	protected function checkAccess() {
		if(Yii::app()->user->isGuest || !User::model()->findByUid(Yii::app()->user->id)->isAdmin)
			throw new CHttpException(403, Yii::t('sprint', 'Access denied'));
	}
}