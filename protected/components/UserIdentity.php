<?php
Class UserIdentity Extends CBaseUserIdentity {
	protected $_uid;
	protected $_password;
	protected $_user;

	public function __construct($uid, $password) {
		$this->uid = $uid;
		$this->password = $password;
	}

	public function authenticate() {
		$user = User::model()->findByUid($this->uid);
		$this->_user = $user;

		if(!$user)
			return $this->errorCode = self::ERROR_USERNAME_INVALID;

		$user->password = $this->password;

		$auth = $user->auth();

		if($auth)
			$this->errorCode = self::ERROR_NONE;
		else
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
	}

	public function getId() {
		return $this->_uid;
	}

	public function setId($id) {
		$this->uid = $id;
	}

	public function getUid() {
		return $this->_uid;
	}

	public function setUid($uid) {
		$this->_uid = $uid;
	}

	public function getPassword() {
		return $this->_password;
	}

	public function setPassword($password) {
		$this->_password = $password;
	}

	public function getName() {
		return $this->_user->cn;
	}
}
?>