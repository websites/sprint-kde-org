<?php
Class Formatter Extends CFormatter {
	public function formatDate($date) {
		if($date) //dont show date for 0 timestamp (1 Jan 1970)
			return parent::formatDate($date);
	}
}