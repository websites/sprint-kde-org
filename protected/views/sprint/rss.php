<?php
$sprints = Sprint::model()->findAll(array(
    'condition' => 'start_date > '.time(),
    'order' => 'start_date'
));

echo '<?xml version="1.0" encoding="UTF-8" ?>';
?>
<rss version="2.0">
<channel>
	<title><?php echo Yii::t('sprints', 'Upcoming KDE sprints'); ?></title>
	<description></description>
	<link><?php echo Yii::app()->createAbsoluteUrl('/'); ?></link>
	<lastBuildDate><?php echo date('r', time()); ?></lastBuildDate>
	<pubDate><?php ?></pubDate>

	<?php
	foreach($sprints as $sprint) {
	?>
		<item>
			<title><?php
				if($sprint->start_date && $sprint->remainingDays > 0)
					echo Yii::t('sprints', ':name (in :days days)', Array(
						':name' => $sprint->name,
						':days' => $sprint->remainingDays
					));
				else if($sprint->start_date && $sprint->remainingDays < 0)
					echo Yii::t('sprints', ':name (:days days ago)', Array(
						':name' => $sprint->name,
						':days' => $sprint->remainingDays*-1
					));
				else
					echo Yii::t('sprints', ':name', Array(
						':name' => $sprint->name
					));
				?>
			</title>
			<description>
				<![CDATA[
				<?php echo $sprint->description; ?>
				]]>
			</description>
			<link><?php echo Yii::app()->createAbsoluteUrl('sprint/view', array('id' => $sprint->id));?></link>
			<guid><?php echo $sprint->id;?></guid>
			<pubDate><?php echo date('r', $sprint->start_date);?></pubDate>
		</item>
	<?php } ?>
</channel>
</rss>