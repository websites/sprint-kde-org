<a href="<?php echo $this->createUrl('/sprint/create'); ?>">
	<?php echo Yii::t('sprint', 'Add a sprint'); ?>
</a>
<br />

<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => New CActiveDataProvider('Sprint'),
	'columns' => Array(
		'name',
		'location.country',
		'location.city',
		'start_date:date',
		'end_date:date',
		array(
			'class' => 'CButtonColumn'
		)
	)
));