<div style="display: table; width:100%">
	<?php $form = $this->beginWidget('CActiveForm'); ?>
		<?php echo Yii::t('sprints', 'Sending emails might take a while. After submitting the form, do not refresh the page.'); ?>
		<br /><br />
		<?php echo Yii::t('sprints', 'You can use the following variables in your message body:'); ?>
		<br />
		<table>
			<thead>
				<tr>
					<td>
						<?php echo Yii::t('sprints', 'Variable name'); ?>
					</td>
					<td>
						<?php echo Yii::t('sprints', 'Description'); ?>
					</td>
					<td>
						<?php echo Yii::t('sprints', 'Example result'); ?>
					</td>
				</tr>
			</thead>

			<tbody>
				<?php
				$allParticipants = $model->sprint->participants;
				$exampleRow = current($allParticipants)->user;

				foreach($model->userAttributes as $attribute)
					echo "<tr><td>:{$attribute}</td><td>".$exampleRow->getAttributeLabel($attribute)."</td><td>{$exampleRow->{$attribute}}";
				?>
			</tbody>
		</table>
		<br />

		<div class="row">
			<?php echo $form->labelEx($model, 'subject'); ?>
			<br />
			<?php echo $form->textField($model, 'subject'); ?>
			<br />
			<?php echo $form->error($model, 'subject'); ?>
		</div>

		<div class="row">
			<?php echo $form->labelEx($model, 'body'); ?>
			<br />
			<?php echo $form->textArea($model, 'body'); ?>
			<br />
			<?php echo $form->error($model, 'body'); ?>
		</div>

		<div class="row">
			<?php echo CHtml::submitButton(); ?>
		</div>

	<?php $this->endWidget(); ?>

	<div style="float:right; text-align: center; font-size: 12px;">
		<?php
		$to = '';
		foreach($model->sprint->participants as $participant)
			$to .= $participant->user->mail.',';
		?>
		<a href="mailto:<?php echo $to;?>">
			<img src="<?php echo Yii::app()->baseUrl;?>/css/images/email-client.png" />
			<br />
			Use my email-client instead.
		</a>
	</div>

</div>