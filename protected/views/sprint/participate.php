<?php $form = $this->beginWidget('CActiveForm'); ?>

	<?php
	if($message)
		echo "<div class='box'>{$message}</div>";
	?>

	<h1>
		<?php echo Yii::t('sprint', 'Project/work information'); ?>
	</h1>

	<?php if(count($model->sprint->groups)) { ?>
		<div class="row">
			<?php echo $form->labelEx($model, 'group_id'); ?>
			<br />
			<?php echo $form->dropDownList($model, 'group_id', CHtml::listData($model->sprint->groups, 'id', 'name')); ?>
			<br />
			<?php echo $form->error($model, 'group_id'); ?>
		</div>
	<?php } ?>

	<div class="row">
		<fieldset>
			<legend><?php echo $form->label($model, 'todos'); ?></legend>
			<?php
			$deleteButton = CHtml::button('X', array('class' => 'deleteGroup', 'onclick' => 'js:$(this).parents("li").detach();')).'&nbsp; ';
			?>
			<ul id="todos">
				<?php foreach($model->todos as $todo) { ?>
					<li>
						<?php
						echo $deleteButton;
						echo CHtml::textField('Participant[todos]['.$todo->id.']', $todo->todo, array('class' => 'todo'));
						?>
					</li>
				<?php } ?>
			</ul>
			<br />
			<?php echo CHtml::button('Add', array('id' => 'addTodo')); ?>
			<br />
			<div style="font-size: 11px;">
				<?php echo Yii::t('sprints', '(Make sure you save the form after adding items)'); ?>
			</div>
		</fieldset>
	</div>

	<h1>
		<?php echo Yii::t('sprint', 'Sponsoring information (private)'); ?>
	</h1>

	<div class="row">
		<?php echo Yii::t('sprints', 'What do you need sponsored? <a href="https://ev.kde.org/rules/sprint_policy.php">KDE Sprint Policy</a>'); ?>
		<br />
		<table id="sponsorships" border="0" style="width: auto;">
			<?php foreach($model->sponsorships as $sponsorship_value => $sponsorship_name) { ?>
				<?php $id = 'sponsorship_cost_'.$sponsorship_value; ?>
				<tr>
					<td>
						<?php echo CHtml::checkBox("sponsorship[$sponsorship_value]", $model->{$id}).'&nbsp;'.$sponsorship_name; ?>
					</td>
					<td <?php if( !($model->{$id}) ) echo 'style="display:none;"'; ?>>
						<?php echo $form->textField($model, $id, array('placeholder' => Yii::t('sprints', 'Budgeted cost'))); ?> €
						<?php if($model->hasErrors($id)) { ?>
							<br />
							<?php echo $form->error($model, $id); ?>
						<?php } ?>
					</td>
				</tr>
			<?php }  ?>
		</table>
	</div>

	<h1>
		<?php echo Yii::t('sprints', 'Travel and accommodation information'); ?>
	</h1>

	<div class="row">
		<?php echo $form->labelEx($model, 'arrival_date'); ?>
		<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', Array(
			'name' => 'Participant[arrival_date]',
			'value' => $model->arrival_date ? Yii::app()->format->formatDate($model->arrival_date) : ''
		)); ?>
		<br />
		<?php echo $form->error($model, 'arrival_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'departure_date'); ?>
		<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', Array(
			'name' => 'Participant[departure_date]',
			'value' => $model->departure_date ? Yii::app()->format->formatDate($model->departure_date) : ''
		)); ?>
		<br />
		<?php echo $form->error($model, 'departure_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'needs_accommodation'); ?>
		<br />
		<?php echo $form->checkBox($model, 'needs_accommodation'); ?>
		<br />
		<?php echo $form->error($model, 'needs_accommodation'); ?>
	</div>

	<div id="share_room_container" class="row" style="<?php if(!$model->needs_accommodation) echo 'display:none;';?>">
		<?php echo $form->labelEx($model, 'share_room'); ?>
		<br />
		<?php echo $form->checkBox($model, 'share_room'); ?>
		<br />
		<?php echo $form->error($model, 'share_room'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'food_preferences'); ?>
		<br />
		<?php echo $form->dropDownList($model, 'food_preferences', $model->foods); ?>
		<br />
		<?php echo $form->error($model, 'group_id'); ?>
	</div>

	<h1>
		<?php echo Yii::t('sprints', 'Additional comments (private)'); ?>
	</h1>

	<div class="row">
		<?php echo $form->textArea($model, 'additional_comments'); ?>
		<br />
		<?php echo $form->error($model, 'additional_comments'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'public'); ?>
		<br />
		<?php echo $form->checkBox($model, 'public', array(
			'checked' => 'checked'
        )); ?>
		<br />
		<?php echo $form->error($model, 'public'); ?>
	</div>

	<div class="row">
		<?php echo CHtml::submitButton(Yii::t('sprints', 'Save')); ?>
		&nbsp;
		<?php
		if(!$model->isNewRecord)
			echo CHtml::submitButton(Yii::t('sprints', 'Remove me'), array('name' => 'delete'));
		?>
	</div>
<?php $this->endWidget(); ?>

<script type="text/javascript">
	$(document).ready(function() {
	$('input[name=needs_sponsorship]').click(function() {
			if($(this).attr('checked'))
					$('#sponsorship_details').slideDown();
			else
					$('#sponsorship_details').slideUp().find('input').val('').removeAttr('checked');
	});

	$('#Participant_needs_accommodation').click(function() {
			if($(this).attr('checked'))
					$('#share_room_container').slideDown();
			else {
					$('#share_room_container').slideUp();
					$('#share_room_container input').removeAttr('checked');
			}
	});

		var addTodo = function() {
			var item = $('<li><?php echo $deleteButton;?><input class="todo" type="text" name="Participant[new_todos][]" value="" /></li>');
			item.appendTo($('#todos'));
			item.find('input[type="text"]').val('');
			item.find('input[type="text"]').focus();
			captureKeys(item.find('input[type="text"]'));
		};

		$('#addTodo').click(addTodo);

		function captureKeys(input) {
			input.keydown(function(e) {
				//Enter or ',' are pressed. Add new one

				if(e.keyCode === 13 || e.keyCode === 188) {
					addTodo();
					e.preventDefault();
				}
			});
		}
		captureKeys($('.todo'));
	});

	$('#sponsorships input[type="checkbox"]').click(function() {
		var cost = $(this).parent().parent().find('td:last');

		if($(this).attr('checked')) {
			cost.show();
		} else {
			cost.hide();
			cost.find('input').val('');
		}
	});
</script>
