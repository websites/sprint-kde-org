<?php
$userAccount = User::model()->findByUid(Yii::app()->user->id);

if( $userAccount instanceof User && $userAccount->isAdmin ) {
	Yii::app()->clientScript->registerCoreScript('jquery');
	?>
	<script type="text/javascript">
		$(document).ready(function() {
			$('.manual_add').click(function() {
				var uid = window.prompt('Enter the username of the person you want to add');
				if(!uid)
					return ;

				window.location = "<?php echo $this->createUrl('/sprint/participant', array('id' => $model->id));?>?uid="+uid;
			});
		});
	</script>
	<?php
}
?>

<div id="sprint">
	<h1 class="name">
		<?php echo $model->name; ?>
	</h1>

	<?php if ($model->start_date && $model->end_date) { ?>
		<h2>
			<?php
			$formatter = New CFormatter;
			$formatter->dateFormat = 'd M';

			echo Yii::t('sprint', ':from to :to; :days days from now.', array(
				':from' => $formatter->date($model->start_date),
				':to' => $formatter->date($model->end_date),
				':days' => $model->remainingDays
			)); ?>
		</h2>
	<?php } else
		echo Yii::t('sprints', 'Sprint dates are not decided yet'); ?>

	<br />

	<?php if(count($model->coordinators)) { ?>
		<div class="box">
			<h2>
				<?php echo Yii::t('sprint', 'Coordinators'); ?>
			</h2>

			<ul>
				<?php foreach($model->coordinators as $coordinator) { ?>
					<li>
						<?php echo Yii::t('sprint', ':name (:role)', array(':name' => $coordinator->user->fullName, ':role' => $coordinator->role)); ?>
					</li>
				<?php } ?>
			</ul>
		</div>
	<?php } ?>

	<div class="description">
		<?php echo $model->description; ?>
	</div>


	<br />

	<?php if($model->location) { ?>
		<h1>
			<?php echo Yii::t('sprint', 'Location information'); ?>
		</h1>
		<h2>
			<?php echo Yii::t('sprint', ':city, :country', array(':city' => $model->location->city, ':country' => $model->location->country)); ?>
		</h2>
		<br />

		<div class="location">
			<?php if($model->location->longitude && $model->location->latitude) { ?>
				<iframe class="map" width="425" height="350" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.openstreetmap.org/export/embed.html?bbox=<?php echo $model->location->longitude - 0.007; ?>,<?php echo $model->location->latitude - 0.007; ?>,<?php echo $model->location->longitude + -0.007; ?>,<?php echo $model->location->latitude + 0.007; ?>&amp;layer=mapnik"></iframe>
			<?php } ?>

			<br />
			<div class="box address">
				<?php echo Yii::t('sprint', ':address, :city, :country.', array(
					':address' => $model->location->address,
					':city' => $model->location->city,
					':country' => $model->location->country
				)); ?>
			</div>
		</div>
		<div class="description">
			<?php echo $model->location->description; ?>
		</div>
		<br />
	<?php } else
		echo Yii::t('sprints', 'Sprint location is not decided yet.'); ?>

	<?php if(!$model->closed) { ?>
		<?php if($model->isParticipant(Yii::app()->user->id)) { ?>
			<a href="<?php echo $this->createUrl('/sprint/participate', array('id' => $model->id));?>">
				<span class="button">
					<?php echo Yii::t('sprint', 'Edit my information'); ?>
				</span>
			</a>
		<?php } else { ?>
			<a href="<?php echo $this->createUrl('/sprint/participate', array('id' => $model->id));?>">
				<span class="button">
					<?php echo Yii::t('sprint', 'Participate'); ?>
				</span>
			</a>
		<?php } ?>
	<?php } ?>

	<?php if($model->isCoordinator(Yii::app()->user->id) || ($userAccount instanceof User && $userAccount->isAdmin) ) { ?>
		<a href="<?php echo $this->createUrl('/sprint/update', array('id' => $model->id));?>">
			<span class="button">
				<?php echo Yii::t('sprint', 'Edit sprint (:name)', array(':name' => $model->name)); ?>
			</span>
		</a>

		<?php if($model->location) { ?>
			<a href="<?php echo $this->createUrl('/location/update', array('id' => $model->location->id));?>">
				<span class="button">
					<?php echo Yii::t('sprint', 'Edit location (:name)', array(':name' => $model->location->name)); ?>
				</span>
			</a>
		<?php } ?>

		<a href="<?php echo $this->createUrl('/sprint/email', array('id' => $model->id));?>">
			<span class="button">
				<?php echo Yii::t('sprint', 'Send email to participants'); ?>
			</span>
		</a>

		<span class="button manual_add">
			<?php echo Yii::t('sprint', 'Manually add participant'); ?>
		</span>
	<?php } ?>

	<div class="clear"></div>

	<?php if (count($model->participants) > 0) { ?>
		<h1>
			<?php echo Yii::t('sprints', 'Participants'); ?>
		</h1>

		<table id="participants">
			<thead>
				<tr>
					<td>
						#
					</td>
					<td></td>
					<td>
						<a href="<?php echo $this->createUrl('/sprint/view/', array('id' => $model->id, 'sort' => 'user.cn'));?>">
							<?php echo Yii::t('sprints', 'Name'); ?>
						</a>
					</td>

					<?php if(count($model->groups)) { ?>
						<td>
							<a href="<?php echo $this->createUrl('/sprint/view/', array('id' => $model->id, 'sort' => 'group.name'));?>">
								<?php echo Yii::t('sprints', 'Group'); ?>
							</a>
						</td>
					<?php } ?>

					<td>
						<?php echo Yii::t('sprints', 'Plans to do'); ?>
					</td>

					<td>
						<a href="<?php echo $this->createUrl('/sprint/view/', array('id' => $model->id, 'sort' => 'arrival_date'));?>">
							<?php echo Yii::t('sprints', 'Arrival'); ?>
						</a>
					</td>

					<td>
						<a href="<?php echo $this->createUrl('/sprint/view/', array('id' => $model->id, 'sort' => 'departure_date'));?>">
							<?php echo Yii::t('sprints', 'Departure'); ?>
						</a>
					</td>

					<td>
						<a href="<?php echo $this->createUrl('/sprint/view/', array('id' => $model->id, 'sort' => 'needs_accommodation'));?>">
							<?php echo Yii::t('sprints', 'Needs accommodation?'); ?>
						</a>
					</td>

					<td>
						<a href="<?php echo $this->createUrl('/sprint/view/', array('id' => $model->id, 'sort' => 'share_room'));?>">
							<?php echo Yii::t('sprints', 'Shares room?'); ?>
						</a>
					</td>

					<td>
						<a href="<?php echo $this->createUrl('/sprint/view/', array('id' => $model->id, 'sort' => 'food_preferences'));?>">
							<?php echo Yii::t('sprints', 'Food'); ?>
						</a>
					</td>

					<?php if($model->isCoordinator(Yii::app()->user->id) || ($userAccount instanceof User && $userAccount->isAdmin) ) { ?>
						<td>
							<?php echo Yii::t('sprints', 'Sponsorship'); ?>
						</td>

						<td>
							<?php echo Yii::t('sprints', 'Comments'); ?>
						</td>

						<td></td>
					<?php } ?>
				</tr>
			</thead>
			<?php
			$formatter = New CFormatter;
			$formatter->dateFormat = 'd F';

			$conditions = 'sprint_id = '.$model->id;

			if(!$model->isCoordinator(Yii::app()->user->id) && !($userAccount instanceof User && $userAccount->isAdmin) && !$model->isParticipant(Yii::app()->user->id)) {
				$conditions .= ' AND public = 1';

				$message = Yii::t('sprints', ':count member(s) are not shown due to privacy preferences.', array(
					':count' => Participant::model()->countByAttributes(array(
						'sprint_id' => $model->id,
						'public' => 0
					))
				));
			}

			$dataProvider = New CActiveDataProvider('Participant', array(
				'criteria' => Array(
					'condition' => $conditions
				),

				'pagination' => False
			));

			function sort_participants($a, $b) {
				$key = $_GET['sort'];
				if(!$key)
					$key = 'id';

				if(!in_array($key, array('id', 'user.cn', 'group.name', 'sponsorship_cost', 'arrival_date', 'departure_date', 'needs_accommodation', 'share_room', 'food_preferences')))
					$key = 'id';

				$aValue = $a;
				foreach(explode('.', $key) as $sub)
					$aValue = $aValue->{$sub};

				$bValue = $b;
				foreach(explode('.', $key) as $sub)
					$bValue = $bValue->{$sub};

				return strcmp($aValue, $bValue);
			}

			$data = $dataProvider->data;
			@usort($data, 'sort_participants');

			$costs = Array(
				'sponsorship_cost_travel' => 0,
				'sponsorship_cost_accommodation' => 0,
				'total' => 0
			);
			$food_count = array(
				Participant::OMNIVORE => 0,
				Participant::VEGETARIAN => 0,
				Participant::VEGAN => 0,
				Participant::SPECIAL => 0
			);
			$todo_count = 0;
			$accommodation_count = 0;

			foreach($data as $index => $participant) { ?>
				<tr>
					<td>
						<?php echo $index+1; ?>
					</td>
					<td>
						<?php if( isset($participant->user->photo) ) { ?>
							<div class="photo">
								<img src="data:image/jpg;base64,<?php echo base64_encode($participant->user->photo); ?>" />
							</div>
						<?php } ?>
					</td>
					<td>
						<?php
						if( isset($participant->user->gender) ) {
							if($participant->user->gender == User::MALE)
								echo '&#9794;';
							elseif($participant->user->gender == User::FEMALE)
								echo '&#9792;';
						}

						echo '&nbsp;';

						echo isset($participant->user->fullName) ? $participant->user->fullName : "";

						if( isset($participant->user->ircNick) )
							echo "<br />({$participant->user->ircNick})";
						?>
					</td>

					<?php if(count($model->groups)) { ?>
						<td>
							<?php
							print $participant->group->name;
							?>
						</td>
					<?php } ?>

					<td style="font-size: 10px;">
						<?php
						foreach($participant->todos as $todo) {
							echo $todo->todo.'<br />';
							$todo_count++;
						}
						?>
					</td>



					<td>
						<?php
						if($participant->arrival_date)
							echo $formatter->date($participant->arrival_date);
						?>
					</td>

					<td>
						<?php
						if($participant->departure_date)
							echo $formatter->date($participant->departure_date);
						?>
					</td>

					<td class="big">
						<?php
						if($participant->needs_accommodation) {
							echo '&#10004;';
							$accommodation_count++;
						}
						?>
					</td>

					<td class="big">
						<?php
						if($participant->share_room)
							echo '&#10004;';
						?>
					</td>

					<td>
						<?php
							$url = Yii::app()->baseUrl.'/css/images/food';
							if($participant->food_preferences == Participant::OMNIVORE) {
								echo CHtml::image($url.'/meat.png', $participant->foods[$participant->food_preferences], array('title' => $participant->foods[$participant->food_preferences]));
								$food_count[Participant::OMNIVORE]++;
							}

							if($participant->food_preferences == Participant::VEGETARIAN) {
								echo CHtml::image($url.'/vegetable.png', $participant->foods[$participant->food_preferences], array('title' => $participant->foods[$participant->food_preferences]));
								echo CHtml::image($url.'/egg.png', $participant->foods[$participant->food_preferences], array('title' => $participant->foods[$participant->food_preferences]));
								$food_count[Participant::VEGETARIAN]++;
							}

							if($participant->food_preferences == Participant::VEGAN) {
								echo CHtml::image($url.'/vegetable.png', $participant->foods[$participant->food_preferences], array('title' => $participant->foods[$participant->food_preferences]));
								$food_count[Participant::VEGETARIAN]++;
							}
						?>
					</td>

					<?php if($model->isCoordinator(Yii::app()->user->id) || ($userAccount instanceof User && $userAccount->isAdmin) ) { ?>
						<td>
							<span style="float: left;">
								<?php
								if($participant->sponsorship_cost_travel) {
									echo '&#9992;';
									$costs['sponsorship_cost_travel'] += $participant->sponsorship_cost_travel;
									$costs['total'] += $participant->sponsorship_cost_travel;
								}
								?>
							</span>

							<span style="float: right;">
								<?php
								if($participant->sponsorship_cost_accommodation) {
									echo '&#9749;';
									$costs['sponsorship_cost_accommodation'] += $participant->sponsorship_cost_accommodation;
									$costs['total'] += $participant->sponsorship_cost_accommodation;
								}
								?>
							</span>

							<div style="clear: both;"></div>

							<span style="float: left; font-size: 10px;">
								<?php
								if($participant->sponsorship_cost_travel)
									echo $participant->sponsorship_cost_travel.' €';
								?>
							</span>

							<span style="float: right; font-size: 10px;">
								<?php
								if($participant->sponsorship_cost_accommodation)
									echo $participant->sponsorship_cost_accommodation.' €';
								?>
							</span>

							<div style="clear: both;"></div>

							<?php if($participant->sponsorship_cost_accommodation && $participant->sponsorship_cost_travel) { ?>
								<hr />
								<?php echo Yii::t('sprints', 'Total: &nbsp; :total€', array(':total' => $participant->sponsorship_cost_accommodation + $participant->sponsorship_cost_travel)); ?>
							<?php } ?>
						</td>

						<td style="font-size: 10px">
							<?php echo $participant->additional_comments; ?>
						</td>

						<td class="big">
							<a href="<?php echo $this->createUrl('/sprint/participant', array('id' => $model->id, 'uid' => $participant->uid)); ?>">
							 &#9997;
							</a>
						</td>
					<?php } ?>
				</tr>
			<?php } ?>

			<?php if($model->isCoordinator(Yii::app()->user->id) || ($userAccount instanceof User && $userAccount->isAdmin) ) { ?>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<?php if(count($model->groups)) { ?>
					<td></td>
				<?php } ?>
				<td>
					<?php echo Yii::t('sprints', ':count items', array(':count' => number_format($todo_count))); ?>
				</td>

				<td></td>
				<td></td>

				<td>
					<?php echo Yii::t('sprints', ':count people', array(':count' => number_format($accommodation_count))); ?>
				</td>

				<td></td>

				<td style="font-size: 9px;">
					<?php foreach(Participant::model()->foods as $id => $title) { ?>
						<?php if(!$food_count[$id]) continue; ?>
						<?php echo Yii::t('sprints', ':type: :count', array(
							':count' => number_format($food_count[$id]),
							':type' => $title
						)); ?>
						<br />
					<?php } ?>
				</td>

				<td>
					<span style="float: left;">
						<?php echo '&#9992;'; ?>
					</span>

					<span style="float: right;">
						<?php echo '&#9749;'; ?>
					</span>

					<div style="clear: both;"></div>

					<span style="float: left; font-size: 10px;">
						<?php echo $costs['sponsorship_cost_travel'].'€'; ?>
					</span>

					<span style="float: right; font-size: 10px;">
						<?php echo $costs['sponsorship_cost_accommodation'].'€'; ?>
					</span>

					<div style="clear: both;"></div>

					<hr />
					<?php echo Yii::t('sprints', 'Total: &nbsp; :total€', array(':total' => $costs['total'])); ?>
				</td>

				<td></td>
				<td></td>
			</tr>
			<?php } ?>
		</table>
		<br />
	<?php
	}
	if( isset($message) ) {
		echo $message;
	}

	if( $model->isCoordinator(Yii::app()->user->id) || ($userAccount instanceof User && $userAccount->isAdmin) ) { ?>
		<a href="<?php echo $this->createUrl('/sprint/export', array('id' => $model->id)); ?>">
			<?php echo Yii::t('sprints', 'Export to CSV'); ?>
		</a>
	<?php } ?>
</div>
