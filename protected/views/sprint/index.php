<?php
$sprints = Sprint::model()->findAll(array(
	'condition' => 'start_date > '.time(),
	'order' => 'start_date'
));
?>
<?php if(!Yii::app()->user->isGuest) {
	$notification = Notification::model()->findByAttributes(Array(
		'uid' => Yii::app()->user->id
	));
	?>

	<input id="notify" type="checkbox" <?php if($notification) echo 'checked';?>>

	<label for="notify">
		<?php echo Yii::t('sprints', 'Let me know when a new sprint is created.'); ?>
	</label>
<?php } ?>

<a href="<?php echo Yii::app()->createUrl('/sprint/rss');?>">
	<img src="<?php echo Yii::app()->baseUrl;?>/css/images/rss.png" style="float: right;" title="<?php echo Yii::t('sprints', 'Get notified about sprints by rss');?>" />
</a>

<a href="<?php echo Yii::app()->createUrl('/sprint/ics');?>">
	<img src="<?php echo Yii::app()->baseUrl;?>/css/images/ical.png" style="float: right;" title="<?php echo Yii::t('sprints', 'Get calendar file (ics)');?>" />
</a>

<?php
$now = time();
$ongoings = Sprint::model()->findAllByAttributes(Array(), 'start_date <= '.$now.' AND end_date >= '.$now);
if(count($ongoings)) {
	?>
	<h1>
	<?php echo Yii::t('sprint', 'Ongoing sprints'); ?>
	</h1>

	<div class="clear"></div>

	<ul class="sprints">
		<?php foreach($ongoings as $sprint) { ?>
			<li>
				<div class="name">
					<?php
					if($sprint->location)
						echo Yii::t('sprint', ':name (:location)', array(':name' => $sprint->name, ':location' => $sprint->location->name));
					else
						echo $sprint->name;
					?>
				</div>

				<div class="description">
					<?php echo $sprint->description; ?>
				</div>

				<div class="hints">
					<?php echo Yii::t('sprint', 'Participants: :count', array(':count' => count($sprint->participants))); ?>
				</div>

				<a href="<?php echo $this->createUrl('/sprint/view', array('id' => $sprint->id)); ?>">
					<span class="button">
						<?php echo Yii::t('sprint', 'More information'); ?>
					</span>
				</a>
			</li>
		<?php } ?>
	</ul>
<?php } ?>

<h1>
<?php echo Yii::t('sprint', 'Upcoming sprints'); ?>
</h1>

<div class="clear"></div>

<ul class="sprints">
	<?php foreach($sprints as $sprint) { ?>
		<li>
			<div class="name">
				<?php
				if($sprint->location)
					echo Yii::t('sprint', ':name (:location)', array(':name' => $sprint->name, ':location' => $sprint->location->name));
				else
					echo $sprint->name;
				?>
			</div>

			<div class="description">
				<?php echo $sprint->description; ?>
			</div>

			<div class="hints">
				<?php echo Yii::t('sprint', 'Participants: :count', array(':count' => count($sprint->participants))); ?>
				<br />
				<?php echo Yii::t('sprint', 'Starts in: :count days (:date)', array(
					':count' => $sprint->remainingDays,
					':date' => Yii::app()->format->formatDate($sprint->start_date)
				)); ?>
			</div>


			<a href="<?php echo $this->createUrl('/sprint/view', array('id' => $sprint->id)); ?>">
				<span class="button">
					<?php echo Yii::t('sprint', 'More information'); ?>
				</span>
			</a>
		</li>
	<?php } ?>
</ul>

<?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
<script type="text/javascript">
	$(document).ready(function() {
		$('#notify').change(function() {
			if($(this).attr('checked'))
				var data = {'enabled' : true}
			else
				var data = {};

			$.ajax({
				'url' : '<?php echo Yii::app()->createUrl('/sprint/notify');?>',
				'data' : data
			});
		});
	});
</script>