BEGIN:VCALENDAR
VERSION:2.0
X-WR-CALNAME:KDE Sprints
PRODID:-//hacksw/handcal//NONSGML v1.0//EN
<?php
foreach(Sprint::model()->findAll() as $sprint) {
if(!$sprint->start_date || !$sprint->end_date)
continue;
?>
BEGIN:VEVENT
UID:<?php echo $sprint->id; ?>

DTSTAMP:<?php echo date('Ymd', $sprint->start_date);?>T000000Z
<?php foreach($sprint->coordinators as $coordinator) { ?>
ORGANIZER;CN=<?php echo $coordinator->user->cn;?>:MAILTO:<?php echo $coordinator->user->mail;?>

<?php } ?>
DTSTART:<?php echo date('Ymd', $sprint->start_date);?>T000000Z
DTEND:<?php echo date('Ymd', $sprint->end_date);?>T000000Z
SUMMARY:<?php echo $sprint->name;?>

<?php if($sprint->location) { ?>
LOCATION:<?php echo $sprint->location->name.', '.$sprint->location->city.','.$sprint->location->country;?>

<?php } ?>
END:VEVENT
<?php } ?>
END:VCALENDAR
