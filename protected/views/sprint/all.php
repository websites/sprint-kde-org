<?php
$formatter = New Formatter;
$formatter->dateFormat = 'j F Y';

Yii::import('zii.widgets.grid.CGridColumn');

Class DurationColumn Extends CGridColumn {
	function renderHeaderCellContent() {
		echo Yii::t('sprints', 'Duration');
	}

	function renderDataCellContent($i, $m) {
		$start = New DateTime('@'.$m->start_date);
		$end   = New DateTime('@'.$m->end_date);
		$diff = $start->diff($end);

		echo Yii::t('sprints', '<b>:days</b> days', array(':days' =>  $diff->format('%a')));
	}
}

$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => New CActiveDataProvider('Sprint', Array(
		'criteria' => Array(
			'order' => 'start_date DESC'
		),
		'pagination' => False
	)),
	'columns' => Array(
		Array(
			'class' => 'CLinkColumn',
			'labelExpression' => '$data->name',
			'urlExpression' => 'Yii::app()->createUrl("/sprint/view", array("id" => $data->id))'
		),
		'location.country',
		'location.city',
		'start_date:date',
		array(
			'class' => 'DurationColumn'
		)
	),
	'formatter' => $formatter
));
?>
<a href="<?php echo Yii::app()->createUrl('/sprint/rss');?>">
	<img src="<?php echo Yii::app()->baseUrl;?>/css/images/rss.png" style="float: right;" title="<?php echo Yii::t('sprints', 'Get notified about sprints by rss');?>" />
</a>

<a href="<?php echo Yii::app()->createUrl('/sprint/ics');?>">
	<img src="<?php echo Yii::app()->baseUrl;?>/css/images/ical.png" style="float: right;" title="<?php echo Yii::t('sprints', 'Get calendar file (ics)');?>" />
</a>