<?php
$form = $this->beginWidget('CActiveForm');
?>
	<div class="row">
		<?php echo $form->labelEx($model, 'name'); ?>
		<br />
		<?php echo $form->textField($model, 'name'); ?>
		<br />
		<?php echo $form->error($model, 'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'description'); ?>
		<br />
		<?php $this->widget('ext.ckeditor.CKEditorWidget', array(
			'model' => $model,
			'attribute' => 'description',
			'defaultValue' => $model->description,
			'config' => Array(
				'contentsCss' => Yii::app()->baseUrl.'/css/style.css',
				'bodyId' => 'WYSIWYG_body'
			)
		));?>
		<br />
		<?php echo $form->error($model, 'description'); ?>
	</div>

	<div class="row">
		<fieldset>
			<legend><?php echo $form->label($model, 'coordinators'); ?></legend>
			<?php
			$deleteButton = CHtml::button('X', array('class' => 'deleteCoordinator', 'onclick' => 'js:$(this).parents("li").detach();')).'&nbsp; ';
			?>
			<?php echo $form->error($model, 'coordinators'); ?>
			<ul id="coordinators">
				<?php foreach($model->coordinators as $coordinator) { ?>
					<li>
						<?php
						echo $deleteButton;
						echo Yii::t('sprint', ':name (:role)', array(':name' => $coordinator->user->cn, ':role' => $coordinator->role));
						echo CHtml::hiddenField('Sprint[coordinators][]', $coordinator->id);
						?>
					</li>
				<?php } ?>
			</ul>
			<br />
			<?php echo Yii::t('sprint', 'Username'); ?>
			<br />
			<?php echo CHtml::textField('coordinator_uid'); ?>
			<br />
			<?php echo Yii::t('sprint', 'Role'); ?>
			<br />
			<?php echo CHtml::textField('coordinator_role'); ?>
			<br />
			<?php echo CHtml::button('Add', array('id' => 'addCoordinator')); ?>
		</fieldset>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'location_id'); ?>
		<br />
		<?php
		$locations = Array(Null => '--');
		foreach(Location::model()->findAll() as $location)
			$locations[$location->id] = $location->name;
		?>
		<?php echo $form->dropDownList($model, 'location_id', $locations); ?>
		<br />
		<?php echo $form->error($model, 'location_id'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'start_date'); ?>
		<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', Array(
			'name' => 'Sprint[start_date]',
			'value' => $model->start_date ? Yii::app()->format->formatDate($model->start_date) : ''
		)); ?>
		<br />
		<?php echo $form->error($model, 'start_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'end_date'); ?>
		<br />
		<?php $this->widget('zii.widgets.jui.CJuiDatePicker', Array(
			'name' => 'Sprint[end_date]',
			'value' => $model->start_date ? Yii::app()->format->formatDate($model->end_date) : ''
		)); ?>
		<br />
		<?php echo $form->error($model, 'end_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'closed'); ?>
		<br />
		<?php echo $form->checkBox($model, 'closed'); ?>
		<br />
		<?php echo $form->error($model, 'closed'); ?>
	</div>

	<div class="row">
		<fieldset>
			<legend><?php echo $form->label($model, 'groups'); ?></legend>
			<?php
			$deleteButton = CHtml::button('X', array('class' => 'deleteGroup', 'onclick' => 'js:$(this).parents("li").detach();')).'&nbsp; ';
			?>
			<ul id="groups">
				<?php foreach($model->groups as $group) { ?>
					<li>
						<?php
						echo $deleteButton;
						echo $group->name;
						echo CHtml::hiddenField('Sprint[groups][]', $group->id);
						?>
					</li>
				<?php } ?>
			</ul>
			<br />
			<?php echo CHtml::textField('group_name'); ?>
			&nbsp;
			<?php echo CHtml::button('Add', array('id' => 'addGroup')); ?>
		</fieldset>
	</div>

	<div class="row">
		<?php echo CHtml::submitButton() ?>
	</div>
<?php
$this->endWidget();
?>
<script type="text/javascript">
	$(document).ready(function() {
		$('#addGroup').click(function() {
			var name = $('#group_name');
			var group = $('<li><?php echo $deleteButton;?>'+name.val()+'<input type="hidden" name="Sprint[new_groups][]" value="'+name.val()+'" /></li>');
			group.appendTo($('#groups'));
			name.val('');
			name.focus();
		});

		$('#addCoordinator').click(function() {
			var coordinator_uid = $('#coordinator_uid');
			var coordinator_role = $('#coordinator_role');
			var group = $('<li><?php echo $deleteButton;?>'+coordinator_uid.val()+' ('+coordinator_role.val()+') <input type="hidden" name="Sprint[new_coordinator_uids][]" value="'+coordinator_uid.val()+'" /><input type="hidden" name="Sprint[new_coordinator_roles][]" value="'+coordinator_role.val()+'" /></li>');
			group.appendTo($('#coordinators'));
			coordinator_uid.val('');
			coordinator_uid.focus();
			coordinator_role.val('');
		});
	});
</script>