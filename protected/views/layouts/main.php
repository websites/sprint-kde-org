<html>
	<head>
		<meta http-equiv="content-type" content="application/xhtml+xml; charset=UTF-8" />
		<title><?php echo Yii::t('sprint', 'KDE Sprints'); ?></title>
		<link rel="stylesheet" href="<?php echo Yii::app()->baseUrl?>/css/style.css" />
		<link rel="alternate" type="application/rss+xml" href="<?php echo Yii::app()->createUrl('/sprint/rss');?>" />
	</head>
	<body>
		<div id="page">
			<div id="header">
				<?php
				$this->widget('zii.widgets.CMenu', array('id' => 'menu', 'items' => $this->_menu));
				?>
				<div id="user">
					<?php if(Yii::app()->user->isGuest) { ?>
						<a href="<?php echo Yii::app()->createUrl('/user/login'); ?>">
							<?php echo Yii::t('sprint', 'Login'); ?>
						</a>
					<?php } else { ?>
						<?php echo Yii::t('sprint', 'Welcome :name.', array(':name' => Yii::app()->user->name)); ?>

						<a href="<?php echo Yii::app()->createUrl('/user/logout'); ?>">
							<?php echo Yii::t('sprint', 'Logout'); ?>
						</a>
					<?php } ?>
				</div>
			</div>

			<div id="breadcrumbs">
				<?php
				if(count($this->breadcrumbs) > 1)
					$this->widget('zii.widgets.CBreadcrumbs', array(
						'links' => $this->breadcrumbs,
						'homeLink' => False
					));
				?>
			</div>

			<div id="content">
				<?php echo $content; ?>
			</div>
		</div>
	</body>
</html>