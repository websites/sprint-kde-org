<?php $form = $this->beginWidget('CActiveForm', array(
	'htmlOptions' => Array(
		'style' => 'display: table; width: 50%;'
	)));
?>

	<a href="http://identity.kde.org">
		<img style="float: right;" src="<?php echo Yii::app()->baseUrl?>/css/images/identity.png" alt="" />
	</a>

	<div class="row">
		<?php echo $form->labelEx($model, 'uid'); ?>
		<br />
		<?php echo $form->textField($model, 'uid'); ?>
		<br />
		<?php echo $form->error($model, 'uid'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'password'); ?>
		<br />
		<?php echo $form->passwordField($model, 'password'); ?>
		<br />
		<?php echo $form->error($model, 'password'); ?>
	</div>

	<div class="row">
		<?php echo CHtml::submitButton(); ?>
	</div>

<?php $this->endWidget();