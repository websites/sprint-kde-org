<?php $form = $this->beginWidget('CActiveForm'); ?>
	<div class="row">
		<?php echo $form->labelEx($model, 'name'); ?>
		<br />
		<?php echo $form->textField($model, 'name'); ?>
		<br />
		<?php echo $form->error($model, 'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'country'); ?>
		<br />
		<?php echo $form->textField($model, 'country'); ?>
		<br />
		<?php echo $form->error($model, 'country'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'city'); ?>
		<br />
		<?php echo $form->textField($model, 'city'); ?>
		<br />
		<?php echo $form->error($model, 'city'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'address'); ?>
		<br />
		<?php echo $form->textArea($model, 'address'); ?>
		<br />
		<?php echo $form->error($model, 'address'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'latitude'); ?>
		<br />
		<?php echo $form->textField($model, 'latitude'); ?>
		<br />
		<?php echo $form->error($model, 'latitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'longitude'); ?>
		<br />
		<?php echo $form->textField($model, 'longitude'); ?>
		<br />
		<?php echo $form->error($model, 'longitude'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model, 'description'); ?>
		<br />
		<?php $this->widget('ext.ckeditor.CKEditorWidget', array(
			'model' => $model,
			'attribute' => 'description',
			'defaultValue' => $model->description,
			'config' => Array(
				'contentsCss' => Yii::app()->baseUrl.'/css/style.css',
				'bodyId' => 'WYSIWYG_body'
			)
		));?>
		<br />
		<?php echo $form->error($model, 'description'); ?>
	</div>

	<div class="row">
		<?php echo CHtml::submitButton(); ?>
	</div>
<?php $this->endWidget(); ?>