<a href="<?php echo $this->createUrl('/location/create'); ?>">
	<?php echo Yii::t('sprint', 'Add a location'); ?>
</a>
<br />

<?php
$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider' => New CActiveDataProvider('Location'),
	'columns' => Array(
		'name',
		array(
			'class' => 'CButtonColumn',
			'template' => '{update}{delete}'
		)
	)
));