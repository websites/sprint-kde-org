<?php
Class LocationController Extends Controller {
	public function actionCreate() {
		$this->checkAccess();
		$model = New Location;

		if( isset($_POST['Location']) ) {
			$model->attributes = $_POST['Location'];

			if($model->save())
				$this->redirect(array('/location/admin'));
		}

		$this->render('create', array('model' => $model));
	}

	public function actionUpdate() {
		$hasAccess = False;
		foreach(Sprint::model()->findAll() as $sprint)
			if($sprint->isCoordinator(Yii::app()->user->id)) {
				$hasAccess = True;
				break;
			}

		if(!$hasAccess)
			$this->checkAccess();

		$model = Location::model()->findByPK($_GET['id']);

		if( isset($_POST['Location']) ) {
			$model->attributes = $_POST['Location'];
			$model->save();
		}

		$this->render('create', array('model' => $model));
	}

	public function actionAdmin() {
		$this->checkAccess();
		$this->render('admin');
	}

	public function actionDelete() {
		$this->checkAccess();
		if( isset($_GET['id']) )
			Location::model()->findByPK($_GET['id'])->delete();
	}
}
