<?php
Class SprintController Extends Controller {
	public function actionCreate() {
		$this->checkAccess();

		$model = New Sprint;

		if( isset($_POST['Sprint']) ) {
			$model->attributes = $_POST['Sprint'];
			$model->start_date = strtotime($_POST['Sprint']['start_date']);
			$model->end_date = strtotime($_POST['Sprint']['end_date']);

			if($model->save()) {
				$this->notify($model);
				$this->updateGroupsAndCoordinators($model);
				$this->redirect(array('/sprint/admin'));
			}
		}

		$this->render('create', array('model' => $model));
	}

	protected function notify($model) {
		Yii::import('ext.yii-mail.*');

		$body = Yii::t('sprints', "A new KDE sprint named ':name' has just been created. View more details on :url .", Array(
			':name' => $model->name,
			':url' => Yii::app()->createAbsoluteUrl('/sprint/view', array('id' => $model->id))
		));

		$mail = New YiiMailMessage;
		$mail->subject = Yii::t('sprints', 'New KDE Sprint');
		$mail->from = 'sprints-admin@kde.org';
		$mail->body = $body;
		$mail->to = Array();

		foreach(Notification::model()->findAll() as $notification)
			$mail->addBcc($notification->user->mail, $notification->user->fullName);

		Yii::app()->mail->send($mail);
	}

	public function actionNotify() {
		if(Yii::app()->user->isGuest)
			throw New CHttpException(403);

		if( isset($_GET['enabled']) ) {
			$n = Notification::model()->findByAttributes(Array(
				'uid' => Yii::app()->user->id
			));
			if($n)
				throw New CHttpException(403);

			$n = New Notification;
			$n->uid = Yii::app()->user->id;
			$n->save();
		} else {
			$notifications = Notification::model()->findAllByAttributes(Array(
				'uid' => Yii::app()->user->id
			));
			foreach($notifications as $n)
				$n->delete();
		}
	}

	public function actionUpdate() {
		$model = Sprint::model()->findByPK($_GET['id']);

		if(Yii::app()->user->isGuest)
			$this->checkAccess();

		if(!$model->isCoordinator(Yii::app()->user->id))
			$this->checkAccess();


		if( isset($_POST['Sprint']) ) {
			$model->attributes = $_POST['Sprint'];
			$model->start_date = strtotime($_POST['Sprint']['start_date']);
			$model->end_date = strtotime($_POST['Sprint']['end_date']);
			$model->save();

			$this->updateGroupsAndCoordinators($model);
		}

		$this->render('create', array('model' => $model));
	}

	protected function updateGroupsAndCoordinators($model) {
		if(count($model->groups) > 0) { //Find out which groups are marked for deletion. delete them.
			$current = Array();
			foreach($model->groups as $group)
				$current[] = $group->id;

			$toBeRemoved = array_diff($current, $_POST['Sprint']['groups']);
			foreach($toBeRemoved as $group_id)
				Group::model()->findByPK($group_id)->delete();
		}

		if(is_array($_POST['Sprint']['new_groups'])) //Save new groups, if there is any.
			foreach($_POST['Sprint']['new_groups'] as $group_name) {
				$group = New Group;
				$group->name = $group_name;
				$group->sprint_id = $model->id;
				$group->save();
			}

		$model->getRelated('groups', True);

		if(count($model->coordinators) > 0) { //Find out which coordinators are marked for deletion. delete them.
			$current = Array();
			$sent = is_array($_POST['Sprint']['coordinators']) ? $_POST['Sprint']['coordinators'] : Array();
			foreach($model->coordinators as $coordinator)
				$current[] = $coordinator->id;

			$toBeRemoved = array_diff($current, $sent);
			foreach($toBeRemoved as $coordinator_id)
				Coordinator::model()->findByPK($coordinator_id)->delete();
		}

		$failed_uids = Array();
		if(is_array($_POST['Sprint']['new_coordinator_uids'])) //Save new coordinators, if there is any.
			foreach($_POST['Sprint']['new_coordinator_uids'] as $index => $coordinator_uid) {
				if(!User::model()->findByUid($coordinator_uid)) {
					$failed_uids[] = $coordinator_uid;
					continue;
				}
				$coordinator = New Coordinator;
				$coordinator->uid = $coordinator_uid;
				$role = $_POST['Sprint']['new_coordinator_roles'][$index];
				$coordinator->role = $role;
				$coordinator->sprint_id = $model->id;
				$coordinator->save();
			}

		if(count($failed_uids))
			$model->addError('coordinators', Yii::t('sprint', 'Couldnt find following username(s): :users', array(':users' => implode(',', $failed_uids))));

		$model->getRelated('coordinators', True);
	}

	public function actionAdmin() {
		$this->checkAccess();
		$this->render('admin');
	}

	public function actionIndex() {
		$this->render('index');
	}

	public function actionAll() {
		$this->render('all');
	}

	public function actionDelete() {
		$this->checkAccess();
		if( isset($_GET['id']) )
			Sprint::model()->findByPK($_GET['id'])->delete();
	}

	public function actionView() {
		$this->render('view', array('model' => Sprint::model()->findByPK($_GET['id'])));
	}

	public function actionExport() {
		$sprint = Sprint::model()->findByPK($_GET['id']);

		if(Yii::app()->user->isGuest)
			$this->checkAccess();

		if(!$sprint->isCoordinator(Yii::app()->user->id))
			$this->checkAccess();

		$item[] = Yii::t('sprints', 'Full Name');
		$item[] = Yii::t('sprints', 'Email address');
		$item[] = Yii::t('sprints', 'IRC nickname');
		$item[] = Yii::t('sprints', 'Gender');
		if(count($sprint->groups))
			$item[] = Yii::t('sprints', 'Group');
		$item[] = Yii::t('sprints', 'Plans to do');
		$item[] = Yii::t('sprints', 'Travel Sponsorship');
		$item[] = Yii::t('sprints', 'Accommodation Sponsorship');
		$item[] = Yii::t('sprints', 'Arrival date');
		$item[] = Yii::t('sprints', 'Departure date');
		$item[] = Yii::t('sprints', 'Shares room?');
		$item[] = Yii::t('sprints', 'Feed preference');
		$item[] = Yii::t('sprints', 'Additional comments');

		$data[] = $item;

		foreach($sprint->participants as $participant) {
			$item = Array();
			$item[] = $participant->user->cn;
			$item[] = $participant->user->mail;
			$item[] = $participant->user->ircNick;

			if($participant->user->gender == User::MALE)
				$item[] = Yii::t('sprints', 'Male');
			elseif($participant->user->gender == User::FEMALE)
				$item[] = Yii::t('sprints', 'Female');
			else
				$item[] = Yii::t('sprints', 'Unknown');

			if(count($sprint->groups))
				$item[] = $participant->group->name;

			$todos = '';
			foreach($participant->todos as $todo)
				$todos .= $todo->todo."\n";
			$item[] = $todos;

			$item[] = ((Float)$participant->sponsorship_cost_travel);
			$item[] = ((Float)$participant->sponsorship_cost_accommodation);

			$item[] = Yii::app()->format->date($participant->arrival_date);
			$item[] = Yii::app()->format->date($participant->departure_date);

			if($participant->share_room)
				$item[] = Yii::t('sprints', 'Yes');
			else
				$item[] = Yii::t('sprints', 'No');

			$item[] = $participant->foods[$participant->food_preferences];

			$item[] = $participant->additional_comments;

			$data[] = $item;
		}

		$csv = '';
		foreach($data as $line) {
			foreach($line as $field)
				$csv .= '"'.$field.'",';
			$csv .= "\n";
		}

		Yii::app()->request->sendFile($sprint->name.'.csv', $csv, 'text/csv');
	}

	public function actionParticipate() {
		if(Yii::app()->user->isGuest)
			$this->redirect(Yii::app()->user->loginUrl);

		$model = Sprint::model()->findByPk($_GET['id']);

		if($model->closed)
			$this->redirect($this->createUrl('/sprint/view', array('id' => $model->id)));

		$this->saveParticipant($_GET['id'], Yii::app()->user->id);
	}

	public function actionParticipant() {
		if(Yii::app()->user->isGuest)
			$this->checkAccess();

		$model = Sprint::model()->findByPK($_GET['id']);

		if(!$model->isCoordinator(Yii::app()->user->id))
			$this->checkAccess();

		$this->saveParticipant($_GET['id'], $_GET['uid']);
	}

	protected function saveParticipant($sprint_id, $uid) {
		$model = $this->getParticipant($sprint_id, $uid);

		if(!$model->user)
			return $this->renderText(Yii::t('sprint', 'User :uid not found.', array(':uid' => $uid)));

		$message = Null;

		if($_POST) {
			if( isset($_POST['delete']) )
				$model->delete() && $this->redirect($this->createUrl('/sprint/view', array('id' => $model->sprint->id)));

			$model->attributes = $_POST['Participant'];

			if( isset($_POST['Participant']['arrival_date']) )
				$model->arrival_date = strtotime($_POST['Participant']['arrival_date']);

			if( isset($_POST['Participant']['departure_date']) )
				$model->departure_date = strtotime($_POST['Participant']['departure_date']);

			if($model->save()) {
				$message = Yii::t('sprints', 'Your information is now saved successfully.');

				if(count($model->todos) > 0) {
					$current = Array();
					foreach($model->todos as $todo) {
						$posted = $_POST['Participant']['todos'][$todo->id];
						if($posted && $posted !== $todo->todo) {
							$todo->todo = $posted;
							$todo->save();
						} elseif (!$posted)
							Todo::model()->findByPK($todo->id)->delete();
					}

					$todos = $_POST['Participant']['todos'] ? $_POST['Participant']['todos'] : Array();
					$toBeRemoved = array_diff($current, $todos);
					foreach($toBeRemoved as $todo_id)
						Todo::model()->findByPK($todo_id)->delete();
				}

				if(is_array($_POST['Participant']['new_todos']))
					foreach($_POST['Participant']['new_todos'] as $todo_text) {
						if(!$todo_text)
							continue;

						$todo = New Todo;
						$todo->todo = $todo_text;
						$todo->participant_id = $model->id;
						$todo->save();
					}

				$model->getRelated('todos', True);
			}
		}

		$this->render('participate', array('model' => $model, 'message' => $message));
	}

	protected function getParticipant($sprint_id, $uid) {
		$model = Participant::model()->findByAttributes(Array(
			'uid' => $uid,
			'sprint_id' => $sprint_id
		));

		if(!$model) {
			$model = New Participant;
			$model->uid = $uid;
			$model->sprint_id = $sprint_id;
		}

		return $model;
	}

	public function actionEmail() {
		$model = Sprint::model()->findByPK($_GET['id']);

		if(Yii::app()->user->isGuest)
			$this->checkAccess();

		if(!$model->isCoordinator(Yii::app()->user->id))
			$this->checkAccess();

		$form = New EmailForm;
		$form->sprint_id = $_GET['id'];

		if($_POST) {
			$form->subject = $_POST['EmailForm']['subject'];
			$form->body = $_POST['EmailForm']['body'];

			if($form->validate()) {
				$form->send();
				$this->render('/message', array('message' => Yii::t('sprint', 'Email has been sent to :count people.', array(':count' => count($form->sprint->participants)))));
				return ;
			}
		}

		$this->render('email', array('model' => $form));
	}

	public function actionRss() {
		$this->renderPartial('rss');
	}

	public function actionIcs() {
		$output = $this->renderPartial('ics', Array(), True);
		Yii::app()->request->sendFile('sprits.ics', $output, 'text/calendar');
	}
}

Class EmailForm Extends CFormModel {
	public $sprint_id;
	public $subject;
	public $body;
	public $userAttributes = Array('cn', 'mail', 'ircNick');

	protected $_sprint;

	public function rules() {
		return Array(
			Array('subject, body, sprint_id', 'required')
		);
	}

	public function getSprint() {
		if($this->_sprint)
			return $this->_sprint;

		return $this->_sprint = Sprint::model()->findByPK($this->sprint_id);
	}

	public function send() {
		Yii::import('ext.yii-mail.*');

		foreach($this->sprint->participants as $participant) {
			$body = $this->body;
			foreach($this->userAttributes as $attribute)
				$body = str_replace(':'.$attribute, $participant->user->{$attribute}, $body);

			$mail = New YiiMailMessage;
			$mail->subject = $this->subject;
			$mail->from = 'sprints-admin@kde.org';
			$mail->to = $participant->user->mail;
			$mail->body = $body;
			$mail->replyTo = User::model()->findByUid(Yii::app()->user->id)->mail;

			Yii::app()->mail->send($mail);
		}
	}
}
