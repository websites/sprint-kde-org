<?php
Class UserController Extends Controller {
	public function actionLogin() {
		$model = User::model();

		if( isset($_POST['User']) ) {
			$identity = New UserIdentity($_POST['User']['uid'], $_POST['User']['password']);
			$identity->authenticate();

			if($identity->errorCode == UserIdentity::ERROR_NONE) {
				Yii::app()->user->login($identity);

				if(Yii::app()->session->get('login_referer'))
					$this->redirect(Yii::app()->session->get('login_referer'));
				else
					$this->redirect(array('/'));
			} else
				$model->addError('uid', Yii::t('sprint', 'Incorrect username and/or password.'));
		} else
			Yii::app()->session->add('login_referer', $_SERVER['HTTP_REFERER']);

		$this->render('login', array('model' => $model));
	}

	public function actionLogout() {
		Yii::app()->user->logout();
		$this->redirect(array('/'));
	}
}
