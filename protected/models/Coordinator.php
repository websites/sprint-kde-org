<?php
Class Coordinator Extends CActiveRecord {
	protected $_user;

	public static function model($class = __CLASS__) {
		return parent::model($class);
	}

	public function getUser() {
		if($this->_user)
			return $this->_user;

		return $this->_user = User::model()->findByUid($this->uid);
	}
}