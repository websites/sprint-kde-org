<?php
include_once(Yii::getPathOfAlias('application.components.Net.LDAP2').'.php');
Class User Extends CModel {
	protected $_uid;
	protected $_password;
	protected $_cn;
	protected $_dn;
	protected $_mail;
	protected $_ircnick;
	protected $_gender;
	protected $_isAdmin = Null;
	protected $_photo;

	const MALE = 'M';
	const FEMALE = 'F';

	public static function model() {
		return New User;
	}

	public function getGender() {
		return $this->_gender;
	}

	public function getIrcnick() {
		return $this->_ircnick;
	}

	public function getDn() {
		return $this->_dn;
	}

	public function getMail() {
		return $this->_mail;
	}

	public function getCn() {
		return $this->_cn;
	}

	public function getUid() {
		return $this->_uid;
	}

	public function setUid($uid) {
		$this->_uid = $uid;
	}

	public function getPassword() {
		return $this->_password;
	}

	public function setPassword($password) {
		$this->_password = $password;
	}

	public function getPhoto() {
		return $this->_photo;
	}

	public function attributeNames() {
		return array('uid', 'password');
	}

	public function attributeLabels() {
		return array(
			'uid' => Yii::t('user', 'User name'),
			'cn' => Yii::t('user', 'Full name'),
			'ircnick' => Yii::t('user', 'Nickname (on irc)'),
			'mail' => Yii::t('user', 'E-Mail address'),
		);
	}

	public function auth() {
		$config = $this->config->toArray();
		$config['binddn'] = $this->dn;
		$config['bindpw'] = $this->password;

		$connect = Net_LDAP2::connect($config);

		return !PEAR::isError($connect);
	}

	public function findByUid($uid) {
		if(!$uid)
			return ;

		$key = 'user_'.$uid;
		$cached = Yii::app()->cache->get($key);

		if($cached)
			return $cached;

		$filter = Net_LDAP2_Filter::create('uid', 'equals',  $uid);

		$search = $this->ldap->search('ou=people,dc=kde,dc=org', $filter);
		if($search->count() < 1)
			return False;

		$user = $search->current();

		$this->uid = $user->getValue('uid', 'single');
		$this->_cn = $user->getValue('cn', 'single');
		$this->_mail = $user->getValue('mail', 'single');
		$this->_dn = $user->dn();
		$this->_gender = $user->getValue('gender', 'single');
		$this->_ircnick = $user->getValue('ircNick', 'single');
		$this->_photo = $user->getValue('jpegPhoto', 'single');

		$groups = $user->getValue('groupMember');
		$this->_isAdmin = (is_array($groups) && in_array('sprint-admins', $groups)) || ($groups == 'sprint-admins') ||
			(is_array($groups) && in_array('ev-board', $groups)) || ($groups == 'ev-board') ||

		Yii::app()->cache->set($key, $this, 3600); //Cache expires in an hour

		return $this;
	}

	protected function getLdap() {
		static $ldap;
		if($ldap)
			return $ldap;

		return $ldap = Net_LDAP2::connect($this->config->toArray());
	}

	protected function getConfig() {
		static $config;
		if($config)
			return $config;

		return $config = New CConfiguration(Yii::getPathOfAlias('application.config.ldap').'.php');
	}

	protected function getIsAdmin() {
		return $this->_isAdmin;
	}

	protected function getFullName() {
		if(Yii::app()->user->isGuest)
			return $this->cn;
		else
			return CHtml::link($this->cn, 'mailto:'.$this->mail);
	}
}