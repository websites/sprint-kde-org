<?php
Class Location Extends CActiveRecord {
	public static function model($class = __CLASS__) {
		return parent::model($class);
	}

	public function rules() {
		return array(
			array('name, description, country, city, location, latitude, longitude, description', 'safe'),
			array('name, country, city, address', 'required')
		);
	}
}