<?php
Class Participant Extends CActiveRecord {

	protected $_user;

	const NONE = 0;
	const TRAVEL        = 'travel';
	const ACCOMMODATION = 'accommodation';

	const VEGETARIAN = 1;
	const VEGAN = 2;
	const SPECIAL = 3;
	const OMNIVORE = 4;

	public static function model($class = __CLASS__) {
		return parent::model($class);
	}

	public function rules() {
		$rules = Array(
			array('group_id, todo, needs_accommodation, share_room, sponsorship_cost, public, food_preferences, additional_comments', 'safe'),
		);

		$sponsorships = Array();
		foreach($this->sponsorships as $id => $name)
			$sponsorships[] = 'sponsorship_cost_'.$id;

		if(count($sponsorships)) {
			$rules[] = Array(implode(',', $sponsorships), 'numerical');
			$rules[] = Array(implode(',', $sponsorships), 'safe');
		}

		return $rules;
	}

	public function attributeLabels() {
		$labels = Array(
			'group_id' => Yii::t('sprints', 'Group'),
			'public' => Yii::t('sprints', 'Show my name on the list on the sprint page'),
			'needs_accommodation' => Yii::t('sprints', 'Do you want your accommodation to be managed by organizers?'),
			'share_room' => Yii::t('sprints', 'Are you ok to share the room with others or something more explicite?'),
			'todos' => Yii::t('sprints', 'Please add at least 3 short work items. These are nice to facilitate the reporting after the event.')
		);

		foreach($this->sponsorships as $id => $name)
			$labels[] = Array('sponsorship_cost_'.$id => Yii::t('sprints', 'Budgeted ":name" costs', array(':name' => $name)));

		return $labels;
	}

	public function relations() {
		return Array(
			'sprint' => array(self::BELONGS_TO, 'Sprint', 'sprint_id'),
			'group' => array(self::BELONGS_TO, 'Group', 'group_id'),
			'todos' => array(self::HAS_MANY, 'Todo', 'participant_id')
		);
	}

	public function getSponsorships() {
		return Array(
			self::TRAVEL => Yii::t('sprints', 'Travel'),
			self::ACCOMMODATION => Yii::t('sprints', 'Accommodation')
		);
	}

	public function getFoods() {
		return Array(
			self::OMNIVORE => Yii::t('sprints', 'I eat everything'),
			self::VEGETARIAN => Yii::t('sprints', 'Vegetarian'),
			self::VEGAN => Yii::t('sprints', 'Vegan'),
			self::SPECIAL => Yii::t('sprints', 'Special (Contact us)')
		);
	}

	public function getUser() {
		if($this->_user)
			return $this->_user;

		return $this->_user = User::model()->findByUid($this->uid);
	}
}
