<?php
Class Sprint Extends CActiveRecord {
	public static function model($class = __CLASS__) {
		return parent::model($class);
	}

	public function attributeLabels() {
		return array(
			'location_id' => Yii::t('sprint', 'Location')
		);
	}

	public function rules() {
		return array(
			array('name, description, location_id, closed', 'safe'),
			array('name', 'required')
		);
	}

	public function relations() {
		return array(
			'location' => array(self::BELONGS_TO, 'Location', 'location_id'),
			'groups' => array(self::HAS_MANY, 'Group', 'sprint_id'),
			'coordinators' => array(self::HAS_MANY, 'Coordinator', 'sprint_id'),
			'participants' => array(self::HAS_MANY, 'Participant', 'sprint_id')
		);
	}

	public function getRemainingDays() {
		return (Int)(($this->start_date - time()) / 86400);
	}

	public function isCoordinator($uid) {
		foreach($this->coordinators as $coordinator)
			if(strtolower($coordinator->uid) == strtolower($uid))
				return True;

		return False;
	}

	public function isParticipant($uid) {
		foreach($this->participants as $participant)
			if($participant->uid == $uid)
				return True;

		return False;
	}
}